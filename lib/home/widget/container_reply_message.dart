import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';


class ContainerReplyMessage extends StatelessWidget {
  const ContainerReplyMessage({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.only(
          left: SizeConfig.horizontal(5), top: SizeConfig.vertical(1)),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          bottomRight: Radius.circular(10),
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
        child: Container(
          padding: EdgeInsets.only(
            left: SizeConfig.safeHorizontal(2.5),
            right: SizeConfig.safeHorizontal(2),
            top: SizeConfig.safeVertical(1),
          ),
          width: SizeConfig.horizontal(50),
          height: SizeConfig.safeVertical(35),
          color: AppColors.pinkky,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [_richtext(context)],
          ),
        ),
      ),
    );
  }
}

///Widget Text
Widget _richtext(BuildContext context) {
  return Column(
    children: [
      Row(
        children: [
          Flexible(
            child: RichText(
              text: TextSpan(
                text: 'Step 1 : ',
                style: TextStyle(
                    color: AppColors.whiteBackground,
                    fontWeight: FontWeight.w500),
                children: <TextSpan>[
                  TextSpan(
                    text:
                        'lorem ipsum dolor sit amet,consectetur adipsicing etil. lorem ipsum dolor sit amet adipsicing consectetur etil.\n',
                    style: TextStyle(
                        color: AppColors.whiteBackground,
                        fontWeight: FontWeight.normal),
                  ),
                  TextSpan(
                    text:
                        '\nStep 2 :',
                    style: TextStyle(
                        color: AppColors.whiteBackground,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          )
        ],
      )
    ],
  );
}
