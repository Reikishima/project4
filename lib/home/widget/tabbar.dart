import 'package:animated_button_bar/animated_button_bar.dart';
import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';
import 'package:health/home/widget/button.dart';

class Tabbar extends StatelessWidget {
  const Tabbar({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: <Widget>[
        SizedBox(
          width: SizeConfig.screenWidth,
          height: SizeConfig.vertical(7),
          child: ButtonsTabBar(
            contentPadding: EdgeInsets.only(
                left: SizeConfig.safeHorizontal(5),
                right: SizeConfig.safeHorizontal(5)),
            backgroundColor: AppColors.pinkky,
            unselectedBackgroundColor: AppColors.whiteBackground,
            unselectedLabelStyle: TextStyle(color: AppColors.darkBackground),
            labelStyle: TextStyle(
                color: AppColors.whiteBackground,
                fontWeight: FontWeight.bold,
                fontSize: 18),
            tabs: const [
              Tab(
                text: "Top",
              ),
              Tab(
                text: "Math",
              ),
              Tab(
                text: 'Algebra',
              ),
              Tab(
                text: 'Geometri',
              ),
              Tab(
                text: 'Trigonometri',
              ),
            ],
          ),
        ),
        SizedBox(
          width: SizeConfig.screenWidth,
          height: SizeConfig.safeVertical(55),
          child: TabBarView(children: <Widget>[
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const Button1(),
            ),
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const Button1(),
            ),
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const Button1(),
            ),
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const Button1(),
            ),
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const Button1(),
            ),
          ])
        )
      ],
    );
  }
}
