import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class ContainerMessageSendReply extends StatelessWidget {
  const ContainerMessageSendReply({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
    padding: EdgeInsets.only(
      right: SizeConfig.horizontal(5),
      top: SizeConfig.vertical(1),
    ),
    child: ClipRRect(
      borderRadius: const BorderRadius.only(
        bottomLeft: Radius.circular(10),
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
      ),
      child: Container(
        padding: EdgeInsets.only(
            left: SizeConfig.safeHorizontal(2),
            top: SizeConfig.safeVertical(1)),
        width: SizeConfig.horizontal(70),
        height: SizeConfig.vertical(4),
        color: AppColors.cyan,
        child: Column(
          children: [
            Row(
              children: [
                RichText(
                 text: TextSpan(
                  text: 'Thank You a Lot!!!',
                    style: TextStyle(
                        color: AppColors.whiteBackground,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        fontFamily: 'NunitoRegular'),
                 ),
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
  }
}
