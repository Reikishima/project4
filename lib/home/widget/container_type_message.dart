import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';


class ContainerTypeMessage extends StatelessWidget {
  const ContainerTypeMessage({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Container(
        width: SizeConfig.horizontal(70),
        height: SizeConfig.vertical(7),
        decoration: BoxDecoration(
          color: AppColors.whiteBackground,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              spreadRadius: 0.3,
              color: AppColors.pinkky,
              blurRadius: 5,
              blurStyle: BlurStyle.solid
            )
          ]
        ),
        child: TextField(
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.attachment_outlined,
              color: AppColors.pinkky,
            ),
            labelStyle: TextStyle(color: AppColors.greyDisabled),
            labelText: 'Message...',
          ),
        ),
      ),
      Container(
        width: SizeConfig.safeHorizontal(15),
        height: SizeConfig.safeVertical(7),
        decoration: BoxDecoration(
            color: AppColors.pinkky,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(blurRadius: 5, color: AppColors.darkBackground)
            ]),
        child: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.send,
            color: AppColors.whiteBackground,
          ),
        ),
      )
    ],
  );
  }
}