import 'package:flutter/material.dart';

import '../../utils/size_config.dart';


class ContainerMessageSendFirst extends StatelessWidget {
  const ContainerMessageSendFirst({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
    padding: EdgeInsets.only(
        right: SizeConfig.horizontal(5), top: SizeConfig.vertical(2)),
    child: Container(
      width: SizeConfig.horizontal(60),
      height: SizeConfig.vertical(25),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          image: const DecorationImage(
              image: AssetImage('assets/images/linear.png'), fit: BoxFit.cover),
          color: Colors.transparent,
          boxShadow: const [
            BoxShadow(blurRadius: 5),
          ]),
    ),
  );
  }
}