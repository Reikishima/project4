
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

import 'package:health/home/widget/text_dan_picture.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../widget/tabbar.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
              systemOverlayStyle: SystemUiOverlayStyle(
                statusBarColor: AppColors.cyan,
              ),
              backgroundColor: AppColors.whiteBackground,
              shadowColor: Colors.transparent,
              leading: Icon(
                Icons.notifications_none_outlined,
                color: AppColors.darkBackground,
              ),
              actions: [
                Icon(
                  Icons.search_outlined,
                  color: AppColors.darkBackground,
                )
              ],
            ),

          body: SafeArea(
            child: DefaultTabController(
              length: 5,
              child: Column(
                children: <Widget>[
                  Container(
                    color: AppColors.whiteBackground,
                    width: SizeConfig.screenWidth,
                    height: SizeConfig.safeVertical(88.75),
                    child: ResponsiveRowColumn(
                      layout: ResponsiveRowColumnType.COLUMN,
                      children: <ResponsiveRowColumnItem>[
                        ResponsiveRowColumnItem(child: _warpper(context))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          ),
    );
  }
}

Widget _warpper(BuildContext context) {
  return SizedBox(
    width: SizeConfig.screenWidth,
    child: Column(
      children:  const [TextandPicture(), Tabbar(), ],
    ),
  );
}


// SafeArea(
//           child: DefaultTabController(
//             length: 5,
//             child: Column(
//               children: <Widget>[
//                 Container(
//                   color: AppColors.darkBackground,
//                   width: SizeConfig.screenWidth,
//                   height: SizeConfig.vertical(80),
//                   child: ResponsiveRowColumn(
//                     layout: ResponsiveRowColumnType.COLUMN,
//                     children: <ResponsiveRowColumnItem>[
//                       ResponsiveRowColumnItem(child: _warpper(context))
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         )