import 'package:flutter/material.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

import 'package:health/Gallery/widget/rowUp.dart';

import 'package:responsive_framework/responsive_row_column.dart';

import '../widget/special_gift.dart';
import '../widget/tabbar_dot.dart';

class Gallery extends StatelessWidget {
  const Gallery({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return KeyboardSizeProvider(
      child: Scaffold(
        body: DefaultTabController(
            length: 4,
            child: Column(
              children: [
                Container(
                  color: AppColors.whiteBackground,
                  child: ResponsiveRowColumn(
                    layout: ResponsiveRowColumnType.COLUMN,
                    children: <ResponsiveRowColumnItem>[
                      ResponsiveRowColumnItem(child: _allcontent(context))
                    ],
                  ),
                )
              ],
            ),
          ),
      ),
    );
  }
}
///Widget semua content wrap
Widget _allcontent(BuildContext context) {
  return Container(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(96.7),
    color: AppColors.blackcoffee,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const RowUp(),
       _texttitle(context),
        _search(context),
       const TabbarDot(),
       const SpecialGift()
      ],
    ),
  );
}

Widget _texttitle(BuildContext context) {
  return Column(
    children: [
    Padding(
      padding: EdgeInsets.only(left: SizeConfig.safeHorizontal(4)),
      child: Row(
        children: [
          Flexible(
            child: RichText(
              text: TextSpan(
                text: 'Find the best\ncoffee for you',
                style: TextStyle(
                    color: AppColors.whiteBackground,
                    fontSize: 35,
                    fontWeight: FontWeight.w700,
                    fontFamily: 'NunitoRegular'),
              ),
            ),
          )
        ],
      ),
    ),
  ]);
}

Widget _search(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
      color: AppColors.blackgrey,
      borderRadius: BorderRadius.circular(15)
    ),
    width: SizeConfig.safeHorizontal(85),
    margin: EdgeInsets.only(
        top: SizeConfig.safeVertical(2), right: SizeConfig.safeHorizontal(5)),
    child: TextField(
      decoration: InputDecoration(
        focusColor: AppColors.greycoffee,
        fillColor: AppColors.greycoffee,
        prefixIcon: Icon(
          Icons.search_rounded,
          color: AppColors.greycoffee,
          size: 25,
        ),
        hintText: 'Find your coffee...',
        hintStyle: TextStyle(color: AppColors.greycoffee, fontSize: 13),
        border: const UnderlineInputBorder(
          borderSide: BorderSide.none,
         
        ),
      ),
    ),
  );
}

