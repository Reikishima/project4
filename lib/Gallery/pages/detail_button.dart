import 'package:buttons_tabbar/buttons_tabbar.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:health/Gallery/widget/bar_icon_container_stack.dart';
import 'package:health/Gallery/widget/button_tab.dart';
import 'package:health/Gallery/widget/container_buy_now.dart';

import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

import '../widget/blur_container_noplugin.dart';
import '../widget/description.dart';

class DetailButton extends StatelessWidget {
  const DetailButton({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
          color: AppColors.blackcoffee,
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight,
          child: SingleChildScrollView(
            child: Column(
              children: [
                _containerStackWrap(context),
                const Description(),
                _containerTextAndTabbar(context),
               const ContainerBuyNow()
              ],
            ),
          ),
        ),
    );
  }
}


/// Widget stack Container
Widget _containerStackWrap(BuildContext context) {
  return Container(
    margin: const EdgeInsets.all(5),
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(60),
    decoration: BoxDecoration(
        color: AppColors.blackcoffee, borderRadius: BorderRadius.circular(40)),
    child: containerStack(context)
  );
}

/// Widget Container Stack
Widget containerStack (BuildContext context){
  return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40),
            image: const DecorationImage(
                image: AssetImage('assets/images/coffee.jpg'),
                fit: BoxFit.cover),
          ),
        ),
       const BarIconContainer(),
        Positioned(
          top: SizeConfig.vertical(40),
          child:  const BlurContainerNoPlugin()),
      ]
    );
}

Widget _containerTextAndTabbar(BuildContext context) {
  return Container(
      margin: const EdgeInsets.all(5),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(10),
      color: AppColors.blackcoffee,
      child: _columnbutton(context));
}

Widget _columnbutton(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Text(
        'Size',
        style: TextStyle(color: AppColors.coklat),
      ),
      const ButtonBarTab()
    ],
  );
}

