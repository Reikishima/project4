import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class BarIconContainer extends StatelessWidget {
  const BarIconContainer({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Positioned(
          top: SizeConfig.vertical(3),
          child: Container(
            width: SizeConfig.screenWidth,
            height: SizeConfig.vertical(7),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [_toplefticon(context), _toprighticon(context)],
            ),
          ),
        );
  }
}

/// Widget left and Right Icon on Container Stack
Widget _toprighticon(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(right: SizeConfig.horizontal(5)),
    width: SizeConfig.horizontal(10),
    height: SizeConfig.vertical(5),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: AppColors.blackcoffee,
      gradient: LinearGradient(colors: [
        AppColors.blackgrey,
        AppColors.blackcoffee,
      ], begin: Alignment.topLeft, end: Alignment.bottomRight),
      boxShadow: [BoxShadow(blurRadius: 3.0, color: AppColors.blackcoffee)],
    ),
    child: Icon(
      Icons.dashboard,
      color: AppColors.greyDisabled,
      size: 20,
    ),
  );
}

Widget _toplefticon(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(left: SizeConfig.horizontal(4)),
    width: SizeConfig.horizontal(10),
    height: SizeConfig.vertical(5),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(
          colors: [AppColors.blackcoffee, AppColors.blackgrey],
          begin: Alignment.topLeft,
          end: Alignment.centerRight),
    ),
    child: IconButton(
      onPressed: () {
        Get.back();
      },
      icon: Icon(
        Icons.arrow_back_ios_new_rounded,
        size: 20,
        color: AppColors.greycoffee,
      ),
    ),
  );
}