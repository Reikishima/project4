import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class ButtonSpecial extends StatelessWidget {
  const ButtonSpecial({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        margin: const EdgeInsets.all(2),
        height: SizeConfig.vertical(10),
        decoration: BoxDecoration(
            color: AppColors.blackcoffee,
            borderRadius: BorderRadius.circular(15)),
        child: _buttonall(context));
  }
}

Widget _textspecial(BuildContext context) {
  return Row(
    children: [
      Flexible(
        child: RichText(
          text: TextSpan(text: '5 Coffee Beans You\n'),
        ),
      )
    ],
  );
}

Widget _buttonall(BuildContext context) {
  return ElevatedButton(
    style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        backgroundColor: Colors.transparent),
    onPressed: () {},
    child: _imagerow(context)
  );
}


Widget _imagerow (BuildContext context){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          width: SizeConfig.horizontal(25),
          height: SizeConfig.vertical(10),
          margin: const EdgeInsets.all(5),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [AppColors.blackgrey, AppColors.blackcoffee],
                  begin: Alignment.topLeft,
                  end: Alignment.centerRight),
              borderRadius: BorderRadius.circular(15),
              image: const DecorationImage(
                  image: AssetImage('assets/images/latte.jpg'),
                  fit: BoxFit.cover)),
        ),
       _textspan(context)
      ],
    );
}

Widget _textspan (BuildContext context){
  return  Column(
          children: [
            Flexible(
              child: RichText(
                text: TextSpan(
                  text: '5 Coffee Beans You\n',
                  style: TextStyle(
                      color: AppColors.whiteBackground,
                      fontWeight: FontWeight.w500,
                      fontSize: 16),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Must Try!\n',
                      style: TextStyle(
                          color: AppColors.whiteBackground,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                     TextSpan(
                      text: '\$',
                      style: TextStyle(
                          color: AppColors.coklat,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                    TextSpan(
                      text: '5.50',
                      style: TextStyle(
                          color: AppColors.whiteBackground,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    )
                  ],
                ),
              ),
            )
          ],
        );
}