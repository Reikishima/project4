import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';


class ButtonEspresso extends StatelessWidget {
  const ButtonEspresso({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: const EdgeInsets.all(5),
      width: SizeConfig.horizontal(35),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
            colors: <Color>[AppColors.blackgrey, AppColors.blackcoffee],
            begin: Alignment.center,
            end: Alignment.bottomRight),
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            backgroundColor: Colors.transparent),
        onPressed: () {
          // Get.to(Navbar1());
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [_stack(context), _textprice(context)],
        ),
      ),
    );
  }
}

Widget _textprice(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Row(
        children: [
          Flexible(
            child: RichText(
              text: TextSpan(
                text: 'Cappuccino\n',
                style: TextStyle(
                    color: AppColors.whiteBackground,
                    fontWeight: FontWeight.w500,
                    fontSize: 16),
                children: <TextSpan>[
                  TextSpan(
                    text: 'With Oat Milk',
                    style: TextStyle(
                        color: AppColors.whiteBackground,
                        fontWeight: FontWeight.w300,
                        fontSize: 12),
                  )
                ],
              ),
            ),
          )
        ],
      ),
      Container(
        margin: EdgeInsets.only(top: SizeConfig.vertical(1.5)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            RichText(
              text: TextSpan(
                text: '\$',
                style: TextStyle(color: AppColors.coklat),
                children: [
                  TextSpan(
                      text: '4.20',
                      style: TextStyle(color: AppColors.whiteBackground))
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.all(1),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppColors.coklat),
              width: SizeConfig.safeHorizontal(10),
              height: SizeConfig.safeVertical(5),
              child: const Icon(Icons.add),
            )
          ],
        ),
      )
    ],
  );
}

Widget _stack(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(
        top: SizeConfig.vertical(2), bottom: SizeConfig.vertical(1.5)),
    decoration: BoxDecoration(
        color: AppColors.blackcoffee, borderRadius: BorderRadius.circular(15)),
    width: SizeConfig.horizontal(30),
    height: SizeConfig.vertical(17),
    child: Stack(children: [
      Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppColors.basicDark,
            image: const DecorationImage(
                image: AssetImage(
                  'assets/images/latte.jpg',
                ),
                fit: BoxFit.cover)),
      ),
      Positioned(
        left: SizeConfig.horizontal(12),
        child: BlurryContainer(
          blur: 15,
          padding: const EdgeInsets.all(4),
          width: SizeConfig.horizontal(14),
          height: SizeConfig.vertical(3),
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(15), topRight: Radius.circular(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                Icons.star,
                color: AppColors.coklat,
                size: 14,
              ),
              Text(
                '4.5',
                style: TextStyle(
                    fontSize: 14,
                    color: AppColors.whiteBackground,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      )
    ]),
  );
}
