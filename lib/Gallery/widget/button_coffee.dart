

import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

import 'button_cappucino.dart';
import 'button_espresso.dart';
import 'button_latte.dart';


class ButtonCoffee extends StatelessWidget {
  const ButtonCoffee({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: const EdgeInsets.all(2),
      color: AppColors.blackcoffee,
      width: SizeConfig.screenWidth,
      height: SizeConfig.safeVertical(35),
      child: _buttonwrap(context)
    );
  }
}


Widget _buttonwrap (BuildContext context){
  return ListView(
        scrollDirection: Axis.horizontal,
        children: const [
          ButtonLatte(),
          ButtonCappuccino(),
          ButtonEspresso()
          ],
      );
}




