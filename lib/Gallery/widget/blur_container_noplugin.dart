import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class BlurContainerNoPlugin extends StatelessWidget {
  const BlurContainerNoPlugin({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ClipRRect(
      borderRadius: BorderRadius.circular(40),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        child: Container(
          padding: const EdgeInsets.all(10),
          height: SizeConfig.vertical(20),
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(
            color: Colors.transparent.withOpacity(0.5),
          ),
          child: Column(
            children: [
              _valuecon(context),
              _valuecon2(context)
            ],
          ),
        ),
      ),
    );
  }
}


Widget _valuecon(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Column(
        children: [
          RichText(
            text: TextSpan(
              text: 'Cappuccino\n',
              style: TextStyle(
                  color: AppColors.whiteBackground,
                  fontSize: 30,
                  fontWeight: FontWeight.w700),
              children: <TextSpan>[
                TextSpan(
                  text: 'With Oat Milk',
                  style: TextStyle(
                      color: AppColors.whiteBackground,
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                )
              ],
            ),
          )
        ],
      ),
      Container(
        color: Colors.transparent,
        width: SizeConfig.horizontal(30),
        height: SizeConfig.vertical(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: SizeConfig.horizontal(12),
              height: SizeConfig.vertical(7),
              decoration: BoxDecoration(
                  color: AppColors.blackcoffee,
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.coffee,
                    color: AppColors.coklat,
                  ),
                  Text(
                    'Coffee',
                    style: TextStyle(
                        color: AppColors.whiteBackground,
                        fontSize: 12,
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
            ),
            Container(
              width: SizeConfig.horizontal(12),
              height: SizeConfig.vertical(7),
              decoration: BoxDecoration(
                  color: AppColors.blackcoffee,
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.water_drop_rounded,
                    color: AppColors.coklat,
                  ),
                  Text(
                    'Milk',
                    style: TextStyle(
                        color: AppColors.whiteBackground,
                        fontSize: 12,
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
            )
          ],
        ),
      )
    ],
  );
}

Widget _valuecon2(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      SizedBox(
        width: SizeConfig.horizontal(28),
        height: SizeConfig.vertical(5),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          Icon(
            Icons.star,
            color: AppColors.coklat,
          ),
          Text(
            '4.5 (6,956)',
            style: TextStyle(
                color: AppColors.whiteBackground, fontWeight: FontWeight.w500),
          )
        ]),
      ),
      Container(
        alignment: Alignment.center,
        width: SizeConfig.horizontal(30),
        height: SizeConfig.vertical(4),
        decoration: BoxDecoration(
            color: AppColors.blackgrey,
            borderRadius: BorderRadius.circular(10)),
        child: Text(
          'Medium Roasted',
          style: TextStyle(
              color: AppColors.whiteBackground,
              fontWeight: FontWeight.w300,
              fontSize: 12),
        ),
      )
    ],
  );
}
