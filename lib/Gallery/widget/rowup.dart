import 'package:flutter/material.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';

class RowUp extends StatelessWidget {
  const RowUp({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(7),
      color: AppColors.blackcoffee,
      child: _icon(context),
    );
  }

  Widget _icon(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: EdgeInsets.only(left: SizeConfig.safeHorizontal(4)),
          child: Container(
            width: SizeConfig.horizontal(10),
            height: SizeConfig.vertical(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: AppColors.blackcoffee,
              gradient: LinearGradient(colors: [
                AppColors.blackgrey,
                AppColors.blackcoffee,
              ], begin: Alignment.topLeft, end: Alignment.bottomRight),
              boxShadow: [
                BoxShadow(blurRadius: 3.0, color: AppColors.blackcoffee)
              ],
            ),
            child: Icon(
              Icons.dashboard,
              color: AppColors.greyDisabled,
              size: 20,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: SizeConfig.safeHorizontal(4)),
          child: CircleAvatar(
            backgroundColor: AppColors.blackcoffee,
            backgroundImage: const AssetImage('assets/images/black.jpg'),
          ),
        )
      ],
    );
  }
}
