import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

import 'button_coffee.dart';

class TabbarDot extends StatelessWidget {
  const TabbarDot({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: <Widget>[
        SizedBox(
          width: SizeConfig.screenWidth,
          height: SizeConfig.safeVertical(7),
          child: TabBar(
            labelPadding: EdgeInsets.only(right: SizeConfig.safeHorizontal(0)),
            unselectedLabelColor: AppColors.greycoffee,
              labelColor: AppColors.coklat,
              indicator: DotIndicator(
                color: AppColors.coklat,
                distanceFromCenter: 16,
                radius: 3,
                paintingStyle: PaintingStyle.fill,
              ),
              tabs: const [
                Tab(
                  text: 'Cappuccino',
                ),
                Tab(
                  text: 'Espresso',
                ),
                Tab(
                  text: 'Latte',
                ),
                Tab(
                  text: 'Flat Weight',
                ),
              ]),
        ),
         SizedBox(
          width: SizeConfig.screenWidth,
          height: SizeConfig.safeVertical(35),
          child: TabBarView(children: <Widget>[
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const ButtonCoffee(),
            ),
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const ButtonCoffee(),
            ),
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const ButtonCoffee(),
            ),
            SizedBox(
              width: SizeConfig.screenWidth,
              child: const ButtonCoffee(),
            ),
          ])
        )
      ],
    );
  }
}
