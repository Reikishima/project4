import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';
import 'package:readmore/readmore.dart';

import '../../utils/app_colors.dart';

class Description extends StatelessWidget {
  const Description({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Container(
    margin: const EdgeInsets.all(5),
    padding: const EdgeInsets.all(3),
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(20),
    color: AppColors.blackcoffee,
    child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Description',
            style: TextStyle(color: AppColors.whiteBackground),
          ),
          _textdesc(context)
        ],
      ),
    ),
  );
  }
}

Widget _textdesc(BuildContext context) {
  return Column(
    children: [
      ReadMoreText(
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae posuere mi. Suspendisse scelerisque venenatis ullamcorper. Ut laoreet ex ac malesuada tristique. Maecenas hendrerit urna nibh, vitae varius ante dignissim vel. Phasellus sem dolor, tincidunt non laoreet quis, congue in ipsum. Suspendisse commodo suscipit porta. Ut eleifend, libero ac ultricies ornare, dolor leo suscipit arcu, ut lobortis arcu dolor vel felis. Maecenas tincidunt cursus massa vel ultrices. Donec ante arcu, finibus nec ultrices sed, hendrerit vitae nulla. Suspendisse tincidunt arcu velit, non congue enim semper non.',
        trimLines: 7,
        style: TextStyle(
            color: AppColors.whiteBackground, fontWeight: FontWeight.w300),
        trimMode: TrimMode.Line,
        trimCollapsedText: '...Read more',
        trimExpandedText: '..Less',
      )
    ],
  );
}