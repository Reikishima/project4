import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class ButtonBarTab extends StatelessWidget {
  const ButtonBarTab({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return DefaultTabController(
      length: 4,
      child: Row(
      children: [
        ButtonsTabBar(
          contentPadding: EdgeInsets.only(
              left: SizeConfig.safeHorizontal(9),
              right: SizeConfig.safeHorizontal(9)),
          borderWidth: 1,
          splashColor: AppColors.blackgrey,
          borderColor: AppColors.coklat,
          backgroundColor: AppColors.blackgrey,
          unselectedBorderColor: Colors.transparent,
          unselectedBackgroundColor: AppColors.blackgrey,
          unselectedLabelStyle: TextStyle(color: AppColors.whiteBackground),
          labelStyle: TextStyle(
              color: AppColors.whiteBackground,
              fontWeight: FontWeight.bold,
              fontSize: 18),
          tabs: const [
            Tab(
              text: "S",
            ),
            Tab(
              text: "M",
            ),
            Tab(
              text: 'L',
            ),
            Tab(
              text: 'XL',
            ),
          ],
        ),
      ],
      ),
    );
  }
}