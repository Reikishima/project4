import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class ContainerBuyNow extends StatelessWidget {
  const ContainerBuyNow({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: const EdgeInsets.all(5),
      padding: const EdgeInsets.all(10),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(10),
      color: AppColors.blackcoffee,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              RichText(
                text: TextSpan(
                    text: 'Price\n',
                    style: TextStyle(color: AppColors.whiteBackground),
                    children: <TextSpan>[
                      TextSpan(
                        text: '\$',
                        style: TextStyle(color: AppColors.coklat, fontSize: 20),
                      ),
                      TextSpan(
                        text: '4.20',
                        style: TextStyle(
                            color: AppColors.whiteBackground, fontSize: 20),
                      )
                    ]),
              )
            ],
          ),
          SizedBox(
            width: SizeConfig.horizontal(60),
            height: SizeConfig.vertical(7),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.coklat,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15))),
              onPressed: () {},
              child: Text(
                'Buy Now',
                style:
                    TextStyle(color: AppColors.whiteBackground, fontSize: 20),
              ),
            ),
          )
        ],
      ),
    );
  }
}
