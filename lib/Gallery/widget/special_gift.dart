import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

import 'button_special.dart';
import 'button_special2.dart';

class SpecialGift extends StatelessWidget {
  const SpecialGift({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: const EdgeInsets.all(4),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(25.2),
      color: AppColors.blackcoffee,
      child: _allwrapper(context),
    );
  }
}

Widget _allwrapper(BuildContext context) {
  return Column(
    children: [_text(context), _listview(context)],
  );
}

Widget _text(BuildContext context) {
  return Container(
    padding: const EdgeInsets.all(4),
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(5),
    color: AppColors.blackcoffee,
    child: Text(
      'Special for you',
      style: TextStyle(
          color: AppColors.whiteBackground,
          fontSize: 20,
          fontWeight: FontWeight.w600),
    ),
  );
}

Widget _listview(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(
        left: SizeConfig.horizontal(10), right: SizeConfig.horizontal(10)),
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(19),
    color: AppColors.blackcoffee,
    child: ListView(
      scrollDirection: Axis.vertical,
      children: const [
        ButtonSpecial(),
        ButtonSpecial2(),
      ],
    ),
  );
}
