import 'package:flutter/material.dart';

class AppColors {
  Color _hex({required String colorCode}) {
    final String containHex = colorCode.toUpperCase().replaceAll('#', '');
    String result = '';
    if (colorCode.length == 7) {
      result = 'FF$containHex';
    }

    return Color(int.parse(result, radix: 16));
  }

  // list of custom colors
  static Color darkBackground = AppColors()._hex(colorCode: '#231F20');
  static Color darkGold = AppColors()._hex(colorCode: '#BB9B49');
  static Color lightGold = AppColors()._hex(colorCode: '#D3BA6D');
  static Color darkerGold = AppColors()._hex(colorCode: '#B48811');
  static Color lighterGold = AppColors()._hex(colorCode: '#EBD197');
  static Color flatGold = AppColors()._hex(colorCode: '#D7B97C');
  static Color greenInfo = AppColors()._hex(colorCode: '#328035');
  static Color redAlert = AppColors()._hex(colorCode: '#EB6B62');
  static Color yellowEvent = AppColors()._hex(colorCode: '#EECC19');
  static Color greyDisabled = AppColors()._hex(colorCode: '#DADADA');
  static Color whiteSkeleton = AppColors()._hex(colorCode: '#EFEFEF');
  static Color whiteBackground = AppColors()._hex(colorCode: '#FFFFFF');
  static Color basicDark = AppColors()._hex(colorCode: '#231F20');
  static Color itemDark = AppColors()._hex(colorCode: '#111111');
  static Color lightDark = AppColors()._hex(colorCode: '#999999');
  static Color rippleColor =
      AppColors()._hex(colorCode: '#EFEFEF').withOpacity(0.20);
  static Color rippleColorDark =
      AppColors()._hex(colorCode: '#999999').withOpacity(0.20);
  static Color redWeekend = AppColors()._hex(colorCode: '#9E1515');
  static Color darkBlue = AppColors()._hex(colorCode: '#232C7C');
  static Color darkFlatGold = AppColors()._hex(colorCode: '#9C875C');
  static Color shimmerBase = AppColors()._hex(colorCode: '#808080');
  static Color shimmerHightlight = AppColors()._hex(colorCode: '#ABABAB');
  static Color pinkky = AppColors()._hex(colorCode: '#ED33B9');
  static Color cyan = AppColors()._hex(colorCode: '#25BBC2');
  static Color lightcyan = AppColors()._hex(colorCode: '#A3D6C1');
  static Color purple = AppColors()._hex(colorCode: '#D44AD6');
  static Color greycoffee = AppColors()._hex(colorCode: '#605A57');
  static Color blackcoffee = AppColors()._hex(colorCode: '#171413');
  static Color blackgrey = AppColors()._hex(colorCode: '#24272e');
  static Color coklat = AppColors()._hex(colorCode: '#9E5E3B');
  static Color trans = AppColors()._hex(colorCode: '#B8C6CD');
  static Color grey = AppColors()._hex(colorCode: '#E6E6E6');
}
