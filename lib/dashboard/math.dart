import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:health/navbar/navbar.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class Math extends StatefulWidget {
  const Math({super.key});

  @override
  State<Math> createState() => _MathState();
}

class _MathState extends State<Math> {
  //viewport as margin left
  final _pageController = PageController(viewportFraction: 0.877);

  double currentPage = 0;

  //indicator handler
  @override
  void initState() {
    // pageController dibuat selalu listening
    //setiap pageView di scroll ke samping maka akan mengambil index page
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page!.toDouble();
        // ignore: avoid_print
        print(currentPage);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHeight,
        color: AppColors.whiteBackground,
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: _contentWrapper(context),
            )
          ],
        ),
      ),
    );
  }
}

Widget _contentWrapper(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.vertical(70),
          color: AppColors.whiteBackground,
          child: ListView(
            children: [_pageView(context)],
          )),
      Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.vertical(30),
        color: AppColors.whiteSkeleton,
        child: Column(
          children: [_text1(context), _dots(context)],
        ),
      )
    ],
  );
}

Widget _pageView(BuildContext context) {
  return Container(
    color: AppColors.darkBackground,
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(70),
    child: PageView(
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      controller: PageController(viewportFraction: 2),
      children: [
        Container(
          margin: EdgeInsets.only(right: SizeConfig.horizontal(15)),
          child: Image.network(
            'https://picsum.photos/id/20/375/400',
            fit: BoxFit.cover,
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: SizeConfig.horizontal(15)),
          width: SizeConfig.horizontal(10),
          height: SizeConfig.vertical(10),
          child: Image.network(
            'https://picsum.photos/id/20/375/400',
            fit: BoxFit.cover,
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: SizeConfig.horizontal(15)),
          width: SizeConfig.horizontal(10),
          height: SizeConfig.vertical(10),
          child: Image.network(
            'https://picsum.photos/id/20/375/400',
            fit: BoxFit.cover,
          ),
        ),
      ],
    ),
  );
}

// Widget _imageUrl(BuildContext context) {
//   return Row(
//     children: [
//       Container(
//        margin: EdgeInsets.only(right: SizeConfig.horizontal(15)),
//         width: SizeConfig.horizontal(20),
//         height: SizeConfig.vertical(10),
//         child: Image.network(
//             'https://picsum.photos/id/20/375/400',
//             fit: BoxFit.cover,
//           ),
//       ),

//       Container(
//         width: SizeConfig.horizontal(10),
//         height: SizeConfig.vertical(10),
//         child: Image.network(
//           'https://picsum.photos/id/20/375/400',
//           fit: BoxFit.cover,
//         ),
//       ),
//       Container(
//         width: SizeConfig.horizontal(10),
//         height: SizeConfig.vertical(10),
//         child: Image.network(
//           'https://picsum.photos/id/20/375/400',
//           fit: BoxFit.cover,
//         ),
//       ),
//     ],
//   );
// }

Widget _text1(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      left: SizeConfig.safeBlockHorizontal,
      top: SizeConfig.vertical(2.5),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Custom math task creation',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: AppColors.greenInfo,
          ),
          textAlign: TextAlign.left,
        ),
        Padding(
          padding: EdgeInsets.only(
            top: SizeConfig.vertical(2.5),
          ),
          child: SizedBox(
            width: SizeConfig.screenWidth,
            height: SizeConfig.vertical(10),
            child: Text(
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: AppColors.darkBackground),
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac mauris sapien. Sed semper, nisl sed fringilla lobortis, neque nisl posuere massa, sit amet tincidunt nulla sem vitae elit.'),
          ),
        ),
      ],
    ),
  );
}

Widget _dots(BuildContext context) {
  double currentPage = 0;
  return Padding(
    padding: EdgeInsets.only(
      top: SizeConfig.vertical(1.5),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: List.generate(
            3,
            (index) {
              return Container(
                margin: EdgeInsets.only(
                  right: SizeConfig.horizontal(5),
                ),
                alignment: Alignment.bottomLeft,
                height: SizeConfig.vertical(3),
                width: SizeConfig.horizontal(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: currentPage == index
                        ? AppColors.darkBackground
                        : AppColors.greyDisabled),
              );
            },
          ),
        ),
        Row(children: [
          Container(
            padding: EdgeInsets.only(
              right: SizeConfig.horizontal(5),
            ),
            width: SizeConfig.horizontal(35),
            height: SizeConfig.vertical(7),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                  backgroundColor: AppColors.redAlert),
              onPressed: () {
                Get.off(const Navbar());
              },
              child: Text(
                'Start',
                style: TextStyle(
                    color: AppColors.whiteBackground,
                    fontSize: 24,
                    fontWeight: FontWeight.w500),
              ),
            ),
          )
        ])
      ],
    ),
  );
}
