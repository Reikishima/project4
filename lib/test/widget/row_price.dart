import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class RowPrice extends StatelessWidget {
  const RowPrice({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(15),
      color: AppColors.whiteBackground,
      child: rowPrice(context),
    );
  }
}

Widget rowPrice(BuildContext context) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [textPrice(context), rotateContainer(context)],
  );
}

Widget textPrice(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Padding(
        padding: EdgeInsets.only(left: SizeConfig.horizontal(7)),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: 'Total Price\n',
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: AppColors.basicDark),
            children: <TextSpan>[
              TextSpan(
                text: '\$24.00',
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: AppColors.basicDark),
              )
            ],
          ),
        ),
      ),
    ],
  );
}

Widget rotateContainer(BuildContext context) {
  return Stack(
    children: [
      Container(
        margin: EdgeInsets.only(left: SizeConfig.horizontal(5)),
        width: SizeConfig.horizontal(16),
        height: SizeConfig.vertical(8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: AppColors.darkBackground),
        transform: Matrix4.rotationZ(45 * 3.14 / 180),
      ),
      Container(
        margin: EdgeInsets.only(right: SizeConfig.horizontal(10)),
        padding: EdgeInsets.only(
            right: SizeConfig.horizontal(15), top: SizeConfig.vertical(5)),
        width: SizeConfig.horizontal(5),
        height: SizeConfig.vertical(10),
        child: Icon(
          Icons.shopping_cart_outlined,
          color: AppColors.whiteBackground,
          size: 30,
        ),
      ),
    ],
  );
}
