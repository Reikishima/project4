import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

// ignore: must_be_immutable
class CarouselImage extends StatelessWidget {
  CarouselController carouselController = CarouselController();
  CarouselImage({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: const EdgeInsets.all(10),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(42),
      child: CarouselSlider(
        disableGesture: true,
        carouselController: carouselController,
        items: [
          Container(
            margin: const EdgeInsets.all(7),
            child: Image.asset(
              'assets/images/Quinoa.jpg',
              fit: BoxFit.cover,
            ),
          ),
          Container(
            margin: const EdgeInsets.all(7),
            child: Image.asset('assets/images/latte.jpg', fit: BoxFit.cover)),
          Container(
            margin: const EdgeInsets.all(7),
            child: Image.asset('assets/images/coffee.jpg', fit: BoxFit.cover)),
        ],
        options: CarouselOptions(
          autoPlay: false,
          aspectRatio: 1/10,

        ),
      ),
    );
  }
}
