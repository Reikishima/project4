import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:health/test/widget/tabbar_view.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class TabbarFood extends StatelessWidget {
  const TabbarFood({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: [wrapTab(context)],
    );
  }
}

/// Widget Wrap value Tab
Widget wrapTab(BuildContext context) {
  return Container(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(85),
    color: AppColors.whiteBackground,
    child: Stack(
      children: [
        _tabView(context),
        Positioned(
          top: SizeConfig.vertical(8),
          child: Row(
            children: [_buttonTabbar(context), _listFilterButton(context)],
          ),
        ),
      ],
    ),
  );
}

/// Widget Value TabView
Widget _tabView(BuildContext context) {
  return SizedBox(
    width: SizeConfig.screenWidth,
    child: TabBarView(
      children: <Widget>[
        SizedBox(
          width: SizeConfig.screenWidth,
          child: const TabbarViewFood(),
        ),
        SizedBox(
          width: SizeConfig.screenWidth,
          child: const TabbarViewFood(),
        ),
        SizedBox(
          width: SizeConfig.screenWidth,
          child: const TabbarViewFood(),
        ),
      ],
    ),
  );
}

/// Widget Icon Filter
Widget _listFilterButton(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: AppColors.whiteBackground,
        borderRadius: BorderRadius.circular(100)),
    width: SizeConfig.horizontal(10),
    height: SizeConfig.vertical(5),
    child: InkWell(
      splashColor: AppColors.greyDisabled,
      onTap: () {},
      child: Icon(
        Icons.filter_list_rounded,
        color: AppColors.basicDark,
      ),
    ),
  );
}

/// Widget Button Tabbar
Widget _buttonTabbar(BuildContext context) {
  return Container(
    color: AppColors.whiteBackground,
    width: SizeConfig.horizontal(90),
    height: SizeConfig.vertical(7),
    child: ButtonsTabBar(
      buttonMargin: const EdgeInsets.all(5),
      contentPadding: EdgeInsets.only(
          left: SizeConfig.safeHorizontal(7),
          right: SizeConfig.safeHorizontal(7)),
      backgroundColor: AppColors.basicDark,
      radius: 20,
      unselectedBackgroundColor: AppColors.grey,
      unselectedLabelStyle: TextStyle(color: AppColors.darkBackground),
      labelStyle: TextStyle(
          color: AppColors.whiteBackground,
          fontWeight: FontWeight.w300,
          fontSize: 14),
      tabs: const [
        Tab(
          text: "Salads",
        ),
        Tab(
          text: "Soups",
        ),
        Tab(
          text: 'Grilled',
        )
      ],
    ),
  );
}
