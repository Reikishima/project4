import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class MainRow extends StatelessWidget {
  const MainRow({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.only(
          top: SizeConfig.vertical(3), bottom: SizeConfig.vertical(3)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(5),
      color: AppColors.whiteBackground,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [drawer(context), search(context)],
      ),
    );
  }
}

Widget drawer(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(left: SizeConfig.horizontal(3)),
    width: SizeConfig.horizontal(10),
    height: SizeConfig.vertical(5),
    color: AppColors.whiteBackground,
    child: Icon(
      Icons.menu,
      color: AppColors.basicDark,
    ),
  );
}

Widget search(BuildContext context) {
  return Container(
    margin: EdgeInsets.only(
      right: SizeConfig.horizontal(3),
    ),
    width: SizeConfig.horizontal(10),
    height: SizeConfig.vertical(5),
    color: AppColors.whiteBackground,
    child: Icon(
      Icons.search_rounded,
      color: AppColors.basicDark,
    ),
  );
}
