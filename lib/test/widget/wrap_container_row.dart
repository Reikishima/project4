import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class WrapContainerRow extends StatelessWidget {
  const WrapContainerRow({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: InkWell(
        onTap: (){},
        child: Container(
          width: SizeConfig.horizontal(90),
          height: SizeConfig.vertical(15),
          color: AppColors.grey,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              decoration: BoxDecoration(
                  color: AppColors.greyDisabled,
                  borderRadius: BorderRadius.circular(100),
                  image: const DecorationImage(
                      image: AssetImage('assets/images/Quinoa.jpg'),
                      fit: BoxFit.cover)),
              width: SizeConfig.horizontal(30),
              height: SizeConfig.vertical(15),
            ),
            textColumnContainer(context),
            iconAddContainer(context),
          ],
        ),
        ),
      ),
    );
  }
}

Widget textColumnContainer(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          RichText(
            text: TextSpan(
              text: 'Chicken Salads\n',
              style: TextStyle(
                  color: AppColors.basicDark,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
              children: <TextSpan>[
                TextSpan(
                  text: 'Chicken With Avocado\n',
                  style: TextStyle(
                      color: AppColors.basicDark,
                      fontSize: 14,
                      fontWeight: FontWeight.w300),
                ),
                TextSpan(
                  text: '\$32.00',
                  style: TextStyle(
                      color: AppColors.basicDark,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                )
              ],
            ),
          ),
        ],
      )
    ],
  );
}

/// Widget Icon Add
Widget iconAddContainer(BuildContext context) {
  return InkWell(
    onTap: (){},
    child: Container(
      width: SizeConfig.horizontal(10),
      height: SizeConfig.vertical(5),
      decoration: BoxDecoration(
        color: AppColors.basicDark,
        borderRadius: BorderRadius.circular(100),
      ),
      child: Icon(
        Icons.add,
        color: AppColors.whiteBackground,
      ),
    ),
  );
}