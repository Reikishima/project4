import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class DescriptionMixed extends StatelessWidget {
  const DescriptionMixed({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.only(
          left: SizeConfig.horizontal(4), top: SizeConfig.vertical(2)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(20),
      color: AppColors.whiteBackground,
      child: _description(context),
    );
  }
}

Widget _description(BuildContext context) {
  return RichText(
      text: TextSpan(
        text:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse efficitur sodales dui, at tempor velit faucibus a. Morbi sed sapien ut orci imperdiet facilisis quis eu turpis. Quisque interdum rutrum justo ac sagittis. Nam at varius massa, et venenatis mauris. Aenean feugiat, nisl id lacinia vulputate, nisi nibh pellentesque magna, quis ultricies ligula mauris eu ex.',
        style: TextStyle(
          color: AppColors.basicDark,
        ),
      ),
    );
}
