import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class RowBarMixed extends StatelessWidget {
  const RowBarMixed({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.only(top: SizeConfig.vertical(2)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(7),
      child: iconRow(context),
    );
  }
}

Widget iconRow(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      IconButton(
        onPressed: () {
          Get.back();
        },
        icon: Icon(
          Icons.arrow_back_ios_new_rounded,
          color: AppColors.basicDark,
        ),
      ),
      IconButton(
        onPressed: () {
          Get.back();
        },
        icon: Icon(
          Icons.more_vert_rounded,
          color: AppColors.basicDark,
        ),
      ),
    ],
  );
}
