import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:health/test/pages/mixed_salad.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class WrapContainerDown extends StatelessWidget {
  const WrapContainerDown({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: const EdgeInsets.all(25),
      margin: EdgeInsets.only(top: SizeConfig.vertical(2)),
      color: AppColors.whiteBackground,
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(45),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Container(
              width: SizeConfig.horizontal(40),
              height: SizeConfig.vertical(40),
              color: AppColors.grey,
              child: ElevatedButton(
                style:
                    ElevatedButton.styleFrom(backgroundColor: AppColors.grey),
                onPressed: () {
                  Get.to(const MixedSalad());
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    imageColumnButton(context),
                    textColumn(context),
                    iconAddContainer(context)
                  ],
                ),
              ),
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Container(
              width: SizeConfig.horizontal(40),
              height: SizeConfig.vertical(40),
              color: AppColors.grey,
              child: ElevatedButton(
                style:
                    ElevatedButton.styleFrom(backgroundColor: AppColors.grey),
                onPressed: () {},
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    imageColumnButton(context),
                    textColumn(context),
                    iconAddContainer(context)
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/// Widget Image Button in wrap Container down
Widget imageColumnButton(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        image: const DecorationImage(
            image: AssetImage('assets/images/Quinoa.jpg'), fit: BoxFit.cover),
        boxShadow: [
          BoxShadow(
              blurRadius: 10,
              spreadRadius: 1,
              color: AppColors.darkBackground,
              offset: Offset.zero)
        ]),
    width: SizeConfig.horizontal(35),
    height: SizeConfig.vertical(17.5),
  );
}

/// Widget Text Button in wrap Container Down
Widget textColumn(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Flexible(
        child: RichText(
          text: TextSpan(
            text: 'Mixed Salad\n',
            style: TextStyle(
                color: AppColors.basicDark,
                fontSize: 16,
                fontWeight: FontWeight.w600),
            children: <TextSpan>[
              TextSpan(
                text: 'Mix Vegetables\n',
                style: TextStyle(
                    color: AppColors.basicDark,
                    fontSize: 14,
                    fontWeight: FontWeight.w300),
              ),
              TextSpan(
                text: '\$24.00',
                style: TextStyle(
                    color: AppColors.basicDark,
                    fontSize: 14,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

/// Widget Icon Add
Widget iconAddContainer(BuildContext context) {
  return InkWell(
    onTap: (){},
    child: Container(
      width: SizeConfig.horizontal(10),
      height: SizeConfig.vertical(5),
      decoration: BoxDecoration(
        color: AppColors.basicDark,
        borderRadius: BorderRadius.circular(100),
      ),
      child: Icon(
        Icons.add,
        color: AppColors.whiteBackground,
      ),
    ),
  );
}
