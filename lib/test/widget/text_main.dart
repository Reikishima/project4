import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class TextMain extends StatelessWidget {
  const TextMain({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.only(bottom: SizeConfig.vertical(10)),
      padding: EdgeInsets.only(left: SizeConfig.horizontal(5)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(8),
      color: AppColors.whiteBackground,
      child: RichText(
        text: TextSpan(
          text: 'Delicious Salads\n',
          style: TextStyle(
            color: AppColors.basicDark,
            fontSize: 25,
            fontWeight: FontWeight.w700,
          ),
          children: <TextSpan>[
            TextSpan(
              text: 'We Made Fresh and Healthy Food',
              style: TextStyle(
                  color: AppColors.basicDark,
                  fontSize: 12,
                  fontWeight: FontWeight.w300),
            )
          ],
        ),
      ),
    );
  }
}
