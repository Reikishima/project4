import 'package:flutter/material.dart';
import 'package:health/test/widget/text_main.dart';
import 'package:health/test/widget/wrap_container_down.dart';
import 'package:health/test/widget/wrap_container_row.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class TabbarViewFood extends StatelessWidget {
  const TabbarViewFood({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SingleChildScrollView(
      child: Container(
          color: AppColors.whiteBackground,
          width: SizeConfig.screenWidth,
          child: Column(
            children: const [TextMain(), WrapContainerRow(), WrapContainerDown()],
          ),
        ),
    );
  }
}
