import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class RowTimer extends StatelessWidget {
  const RowTimer({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.only(right: SizeConfig.horizontal(45)),
      padding: EdgeInsets.only(left: SizeConfig.horizontal(4)),
      width: SizeConfig.horizontal(55),
      height: SizeConfig.vertical(7),
      color: AppColors.whiteBackground,
      child: rowTimer(context),
    );
  }
}

Widget rowTimer(BuildContext context) {
  return Row(children: [
    const Expanded(
      child: Text('Delivery Time'),
    ),
    Row(
      children: const [Icon(Icons.access_time), Text('25 mins')],
    )
  ]);
}
