import 'package:flutter/material.dart';
import 'package:health/utils/size_config.dart';

import '../../utils/app_colors.dart';

class DiamondShape extends ShapeBorder {
  const DiamondShape();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Container(
        margin: EdgeInsets.only(left: SizeConfig.horizontal(5)),
        width: SizeConfig.horizontal(16),
        height: SizeConfig.vertical(8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: AppColors.darkBackground),
        transform: Matrix4.rotationZ(45 * 3.14 / 180),
      );
  }
  
  @override
  // TODO: implement dimensions
  EdgeInsetsGeometry get dimensions => throw UnimplementedError();
  
  @override
  Path getInnerPath(Rect rect, {TextDirection? textDirection}) {
    // TODO: implement getInnerPath
    throw UnimplementedError();
  }
  
  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
    // TODO: implement getOuterPath
    throw UnimplementedError();
  }
  
  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {
    // TODO: implement paint
  }
  
  @override
  ShapeBorder scale(double t) {
    // TODO: implement scale
    throw UnimplementedError();
  }
}