import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:health/test/controller/test_controller.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class AddButton extends StatelessWidget {
  const AddButton({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.only(left: SizeConfig.horizontal(4)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(7),
      color: AppColors.whiteBackground,
      child: wrapButton(context),
    );
  }
}

Widget wrapButton(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [textColumn(context), addButton(context)],
  );
}

Widget textColumn(BuildContext context) {
  return Column(
    children: [
      Flexible(
        child: RichText(
          text: TextSpan(
              text: 'Mediterranean\n',
              style: TextStyle(
                  color: AppColors.basicDark,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
              children: <TextSpan>[
                TextSpan(
                  text: 'Quinoa Salad',
                  style: TextStyle(
                      color: AppColors.basicDark,
                      fontSize: 24,
                      fontWeight: FontWeight.w600),
                )
              ]),
        ),
      )
    ],
  );
}

Widget addButton(BuildContext context) {
  TestController controller = TestController();
  return Container(
    width: SizeConfig.horizontal(35),
    height: SizeConfig.vertical(5),
    color: AppColors.whiteBackground,
    child: Row(
      children: [
        GestureDetector(
          onTap: () => {
            controller.addSalad(),
          },
          child: Container(
            width: 35,
            height: 45,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: AppColors.basicDark),
            child: Icon(
              Icons.add,
              color: AppColors.whiteBackground,
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Obx(
          () => Text(
            controller.salad.toString(),
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        GestureDetector(
          onTap: () => {
            controller.removeSalad(),
          },
          child: Container(
            width: 35,
            height: 45,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: AppColors.basicDark),
            child: Icon(
              Icons.remove,
              color: AppColors.whiteBackground,
            ),
          ),
        ),
      ],
    ),
  );
}
