import 'package:flutter/material.dart';
import 'package:health/test/widget/tabbar_food.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../widget/main_row.dart';

class Test extends StatelessWidget {
  const Test({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        body: DefaultTabController(
      length: 3,
      child: SizedBox(
          child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: wrapperAll(context))
        ],
      )),
    ));
  }
}

Widget wrapperAll(BuildContext context) {
  return Stack(
    children: <Widget>[
      Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.vertical(96.7),
        color: AppColors.whiteBackground,
        child: Column(
          children: const [MainRow(), TabbarFood()],
        ),
      )
    ],
  );
}
