import 'package:flutter/material.dart';
import 'package:health/test/widget/add_button.dart';
import 'package:health/test/widget/description_mixed.dart';
import 'package:health/test/widget/diamond_shape.dart';
import 'package:health/test/widget/image_carousel.dart';
import 'package:health/test/widget/row_bar_mixed_salad.dart';
import 'package:health/test/widget/row_price.dart';
import 'package:health/test/widget/row_timer.dart';
import 'package:health/utils/size_config.dart';
import 'package:responsive_framework/responsive_framework.dart';

class MixedSalad extends StatelessWidget {
  const MixedSalad({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Column(
          children: [
            SizedBox(
              width: SizeConfig.screenWidth,
              height: SizeConfig.screenHeight,
              child: ResponsiveRowColumn(layout: ResponsiveRowColumnType.COLUMN,children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: wrapperMixedAllSalad(context) ),
              ],
              ),
            ),
          ],
        ),
    );
  }
}

Widget wrapperMixedAllSalad (BuildContext context)
{
  return Container(
    child: contentWrap(context),
  );
}

Widget contentWrap (BuildContext context){
  return Column(
    children: [
      const RowBarMixed(),
      CarouselImage(),
      const AddButton(),
      const DescriptionMixed(),
      const RowTimer(),
      const RowPrice()
    ],
  );
}
