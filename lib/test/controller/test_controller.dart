import 'package:get/state_manager.dart';

class TestController extends GetxController{

  var salad = 0.obs;
  var grilled = 0.obs;
  var soups = 0.obs;

  addSalad (){
   
   return salad.value++;
  }
  
  removeSalad (){
    return salad.value--;
  }
   
 addSoups (){
   
   return soups.value++;
  }
  
  removeSoups (){
    return soups.value--;
  }

 adGrilled (){
   
   return grilled.value++;
  }
  
  removeGrilled (){
    return grilled.value--;
  }

  sumTotal (){
    return salad.value + soups.value + grilled.value;
  }
}