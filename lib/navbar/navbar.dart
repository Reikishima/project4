import 'package:floating_navbar/floating_navbar.dart';
import 'package:floating_navbar/floating_navbar_item.dart';
import 'package:flutter/material.dart';

import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

import '../Gallery/pages/gallery.dart';
import '../test/pages/test.dart';
import '../home/pages/home_page.dart';
import '../profile/profile.dart';

class Navbar extends StatelessWidget {
  const Navbar({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: FloatingNavBar(
          horizontalPadding: SizeConfig.horizontal(15),
          color: AppColors.whiteBackground,
          selectedIconColor: AppColors.pinkky,
          unselectedIconColor: AppColors.greyDisabled,
          hapticFeedback: true,
          items: [
            FloatingNavBarItem(
                title: '',
                page: const Home(),
                iconData: Icons.dashboard_rounded),
            FloatingNavBarItem(
                title: '', page: const Gallery(), iconData: Icons.bolt_rounded),
            FloatingNavBarItem(
                title: '',
                page: const Test(),
                iconData: Icons.messenger_outline_sharp),
            FloatingNavBarItem(
                title: '',
                page: const Profile(),
                iconData: Icons.person_rounded),
          ],
        ),
      ),
    );
  }
}
